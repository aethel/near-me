import { Component } from '@angular/core';
import { AuthService } from '../../global/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  public email: string;
  public password: string;
  public error: string = null;
  constructor(public authService: AuthService, private router: Router) { }

  login() {
    this.authService.signInWithEmail(this.email, this.password).then(val => {
      this.router.navigate(['/mainView']);
    }).catch(err => {
      this.error = err.message;
    });
  }
}
