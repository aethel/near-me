const AppConfig = {
  defaultZoom: 18,
  defaultLat: 55.953251,
  defaultLon: -3.188267,
  collection: 'warsaw',
  travelMode: 'WALKING'
};

export const APPCONFIG = AppConfig;
